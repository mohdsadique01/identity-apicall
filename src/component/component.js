import React, { Component } from 'react'
import Index from './Index'
import Post from './Post';
 class component extends Component {
    constructor(props){
        super(props)
        this.state ={
            items: [],
            DataisLoaded: false
        
        };
    }

    componentDidMount() {
        fetch(" https://jsonplaceholder.typicode.com/users")
            .then((res) => res.json())
            .then((json) => {
                this.setState({
                    items: json,
                    DataisLoaded: true
                });

            })
                       
            
    }

  render() {
    const { DataisLoaded, items ,item} = this.state;

    return (
      <div>
        
        {items.map((item)=>{
            return <Index 
            name={item.name}
            email ={item.email}
            number={item.phone}
            street={item.address.street}
            city={item.address.city}
            zipcode ={item.address.zipcode}
            id={item.id}
            />
           
                     
                    
        })}


        
      </div>
    )
  }
}

export default component
