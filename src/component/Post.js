import React, { Component } from 'react'
import "./post.css"
export class Post extends Component {
  render() {
    
    const post = this.props
    return (
      <div className='post'>
        <h3>{post.post.title}</h3>
        <p><strong className='post-clor'>post : </strong></p>{post.post['body']}
      </div>
    )
  }
}

export default Post
