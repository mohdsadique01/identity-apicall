import React, { Component } from 'react'
// import Album from './album'
import "./index.css"
import Post from './Post'

class Index extends Component {
     constructor(props) {
       super(props)
     
       this.state = {
          item:[],
          DataisLoaded:false,
          photo:[]
       }
     }
    
     componentDidMount(){
      let {id}=this.props
      fetch(`https://jsonplaceholder.typicode.com/posts?userId=${id}`)
      .then((res) => res.json())
      .then((json) => {
          this.setState({
              item: json,
              DataisLoaded: true
          });

      })
      fetch(`https://jsonplaceholder.typicode.com/photos?albumId=${id}`)
      .then((res) => res.json())
      .then((json) => {
        console.log('callllll');
        console.log(json,'photott');
          this.setState({
              photo: json,
              DataisLoaded: true
          });

      })


     }



  render() {
    let {item,photo}=this.state
    const {img,name,street,city,zipcode,number,email,post}= this.props
    return (
      <div className='component'>
        <div className='img'>
            <img src={photo[0]?photo[0].url:""} className='image'/><br />
            {/* {photo.map((ele)=>{
          return <Album img={ele}/>
        })}
             */}
            <h2 className='name'>{name}</h2>
    
            <p className='setname'> <strong>Address</strong>:<br />
             <p className='adres'>street: {street} </p>
            <p className='adres'>city:{city}</p>
            <p className='adres'> zipcode:{zipcode}</p> 
             </p> 
      <p className='setname'><strong>Phone number</strong>: {number}</p>
      <p className='setname'><strong>Email</strong>: {email}<br /></p>
     
        
        </div>
      <div>
        {item.map((ele)=>{
          return <Post post={ele}/>
        })}
        </div>
      </div>
    )
  }
}

export default Index
